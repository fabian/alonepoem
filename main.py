#!/usr/bin/env python
import os
import time
import random

from django.utils import simplejson as json
from google.appengine.api import channel
from google.appengine.api import memcache
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp import util


class MainHandler(webapp.RequestHandler):

    def get(self):
        """
        Creates a client_id and a channel token and renders the poem page.
        """
        client_id = 'client-%f-%d' % (time.time(), random.random() * 9999)
        token = channel.create_channel(client_id)

        context = {"token": token, "client_id": client_id}
        content = template.render("templates/index.html", context)

        self.response.out.write(content)

    def post(self):
        """
        Reveives `client_id` and `escaped_message` and sends the escaped
        message to every other connected clients.
        """
        posting_client_id = self.request.get("client_id")
        escaped_message = self.request.get("escaped_message")

        if posting_client_id is None or escaped_message is None:
            self.error(400)

        client_ids = memcache.get("client_ids") or []

        json_data = json.dumps({
            'escaped_message': escaped_message,
            'connected_clients': len(client_ids),
        })

        for client_id in client_ids:
            if client_id != posting_client_id:
                channel.send_message(client_id, json_data)

        self.response.set_status(204)  # No Content


class ChannelConnectedHandler(webapp.RequestHandler):

    def post(self):
        """
        Adds clients to the client list
        """
        client_id = self.request.get('from')

        client_ids = set(memcache.get("client_ids") or [])
        client_ids.add(client_id)
        memcache.set("client_ids", client_ids)


class ChannelDisconnectedHandler(webapp.RequestHandler):

    def post(self):
        """
        Removes clients from the client list
        """
        client_id = self.request.get('from')

        client_ids = memcache.get("client_ids") or []
        client_ids = set(client_ids).difference([client_id])
        memcache.set("client_ids", client_ids)


def main():
    DEBUG = os.environ['SERVER_SOFTWARE'].startswith('Dev')
    application = webapp.WSGIApplication([
        ('/', MainHandler),
        ('/_ah/channel/connected/', ChannelConnectedHandler),
        ('/_ah/channel/disconnected/', ChannelDisconnectedHandler),
    ], debug=DEBUG)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
